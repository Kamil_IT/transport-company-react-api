import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import Navigation from "./Navigation"
import UserComponent from "./UserComponent";

import'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {


  render() {
    return (
      <Router>
        <>
            {<Navigation/>}
            <UserComponent/>
        </>
      </Router>
    );
  }
}

export default App;
