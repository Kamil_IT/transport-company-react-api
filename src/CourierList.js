import React from "react";
import {NavLink} from "react-router-dom";

const CourierList = () => {
    return (
        <>
            <div className="container">
                <div className="text-center">
                    <h1 className="display-4">Courier list</h1>
                </div>
                <table className="table table-striped table-hover table-sm table-responsive-sm" style={{marginTop: 30}}>
                    <thead className="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Role</th>
                        <th>E-mail</th>
                        <th>PhoneNumber</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                </table>
                <div className="new">
                    <NavLink style={{display: "flex", justifyContent: "center"}} to="/courier/new">Create new</NavLink>
                </div>
            </div>
        </>
    )
}

export default CourierList;