import React, {Component} from "react";
import {NavLink} from "react-router-dom";

class UserOrderNew extends Component {

    render() {
        return (
            <div className="d-flex justify-content-center">
                <div className="bg-secondary">
                    <h1 className="display-4 d-flex justify-content-center" style={{paddingTop: 30}}>Create user
                        order</h1>
                    <div className="d-flex justify-content-center" style={{paddingTop: 30}}>
                        <form>
                            <div className="d-flex flex-column">

                                <label htmlFor="user">
                                    Number
                                    <input
                                        type="text"
                                        id="user"
                                        name="username"
                                    />
                                </label>

                                <label htmlFor="email">
                                    Description
                                    <input
                                        type="email"
                                        id="email"
                                        name="email"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Headquarters
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Comments
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <p style={{fontWeight: "bolder"}}>Package:</p>
                                <label htmlFor="password">
                                    Weigth
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Heigth
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Length
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Width
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <p style={{fontWeight: "bolder"}}>Sender Details:</p>
                                <label htmlFor="password">
                                    Name
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Surname
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    City
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Street
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    House number
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Apartment number
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Phone number
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    E-mail
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    District
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <p style={{fontWeight: "bolder"}}>Recipient Details:</p>
                                <label htmlFor="password">
                                    Name
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Surname
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    City
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Street
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    House number
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Apartment number
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    Phone number
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    E-mail
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>
                                <label htmlFor="password">
                                    District
                                    <input
                                        type="password"
                                        id="password"
                                        name="pass"
                                    />
                                </label>


                                <div className="text-center" style={{marginTop: 20}}>
                                    <button className="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="d-flex flex-column">
                        <NavLink to="" style={{textAlign: "center"}}>back to list</NavLink>
                        <NavLink to="" style={{textAlign: "center"}}>logout</NavLink>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserOrderNew;