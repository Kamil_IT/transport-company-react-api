import React from "react";
import {NavLink} from "react-router-dom";

const Navigation = () => (
    <>
        <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{padding:"0 30 0 30"}}>
            <NavLink className="navbar-brand" to="#">
                <div className="logo">
                    <img src={'images/logo.png'} style={{width: 200}} alt=""/>
                </div>
            </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item dropdown">
                        <NavLink className="nav-link dropdown-toggle" to="/user" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            User
                        </NavLink>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <NavLink className="dropdown-item" to="/user">User list</NavLink>
                            <NavLink className="dropdown-item" to="/user/new">Create new</NavLink>

                        </div>
                    </li>
                    <li className="nav-item dropdown">
                        <NavLink className="nav-link dropdown-toggle" to="/user-order" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            User Order
                        </NavLink>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <NavLink className="dropdown-item" to="/user-order">User order list</NavLink>
                            <NavLink className="dropdown-item" to="/user-order/new">User order new</NavLink>
                        </div>
                    </li>

                    <li className="nav-item dropdown">
                        <NavLink className="nav-link dropdown-toggle" to="/courier" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Courier
                        </NavLink>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <NavLink className="dropdown-item" to="{{ path('courier_index') }}">Courier list</NavLink>
                            <NavLink className="dropdown-item" to="{{ path('courier_new') }}">Create new</NavLink>
                        </div>
                    </li>


                    <li className="nav-item dropdown">
                        <NavLink className="nav-link dropdown-toggle" to="/courier-order" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Courier Order
                        </NavLink>

                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <NavLink className="dropdown-item" to="{{ path('courier_order_index') }}">Courier order list for
                                all</NavLink>
                        </div>


                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            <NavLink className="dropdown-item"
                               to="{{ path('courier_order_index_by_courier', {'courierId': twig_service.getCourier.id}) }}">Courier
                                order list for current courier</NavLink>
                        </div>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link disabled" to="#"><i>Name & Surname :</i></NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link disabled" to="#"><i>E-mail :</i> </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link disabled" to="#"><i style={{fontWeight:"bolder"}}>Role : </i></NavLink>
                    </li>
                </ul>
                <NavLink to="{{ path('app_logout')}}">logout</NavLink>
            </div>
        </nav>
    </>

);
 export default Navigation