import React, {Component} from "react";

import {NavLink} from "react-router-dom";

class CourierNew extends Component {

    render() {
        return (
            <>
                <div className="d-flex justify-content-center">
                    <div className="bg-secondary">
                        <h1 className="display-4 d-flex justify-content-center" style={{paddingTop: 30}}>Create new
                            Courier</h1>
                        <div className="d-flex justify-content-center" style={{paddingTop: 30}}>
                            <div className="d-flex flex-column">
                                <label htmlFor="user">
                                    Name
                                    <input
                                        type="text"
                                        id="user"
                                        name="username"
                                    />
                                </label>
                                <label htmlFor="user">
                                    Surname
                                    <input
                                        type="text"
                                        id="user"
                                        name="username"
                                    />
                                </label>
                                <label htmlFor="user">
                                    Password
                                    <input
                                        type="text"
                                        id="user"
                                        name="username"
                                    />
                                </label>
                                <label htmlFor="user">
                                    E-mail
                                    <input
                                        type="text"
                                        id="user"
                                        name="username"
                                    />
                                </label>
                                <label htmlFor="user">
                                    Phone number
                                    <input
                                        type="text"
                                        id="user"
                                        name="username"
                                    />
                                </label>
                                <label htmlFor="user">
                                    District
                                    <input
                                        type="text"
                                        id="user"
                                        name="username"
                                    />
                                </label>
                                <div className="text-center" style={{marginTop: 20}}>
                                    <button className="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex flex-column">
                            <NavLink to="#" style={{textAlign: "center"}}>back to list</NavLink>
                            <NavLink to="#" style={{textAlign: "center"}}>logout</NavLink>
                        </div>
                    </div>
                </div>
            </>
        )
    }

}

export default CourierNew;