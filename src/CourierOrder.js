import React from "react";
// import {NavLink} from "react-router-dom";

const CourierOrder = () => {
    return (
        <>
            <div className="container">
            <div className="text-center">
                <h1 className="display-4">Courier order index</h1>
            </div>
            <table className="table table-striped table-hover table-sm table-responsive-sm" style={{marginTop:30}}>
                <thead className="thead-dark">
                <tr>
                    <th>Id</th>
                    <th>Number</th>
                    <th>Description</th>
                    <th>Comments</th>
                    <th>Headquarters</th>
                    <th>Package</th>
                    <th>SenderDetails</th>
                    <th>RecipientDetails</th>
                    <th>Status</th>
                    <th>Courier</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
            </div>
        </>

    )
}

export default CourierOrder