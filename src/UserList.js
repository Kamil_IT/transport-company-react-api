import React from "react";
import { NavLink } from "react-router-dom";

const UserList = (props) => {
    const {id,name,surname,role,phone_number,username} = props.user
    return (
        <>
            <tbody>
            <tr>
                <td>{id}</td>
                <td>{name}</td>
                <td>{surname}</td>
                <td>{role}</td>
                <td>{phone_number}</td>
                <td>{username}</td>
                <td>
                    <NavLink to=""> show</NavLink>
                    <NavLink to=""> edit</NavLink>
                </td>
            </tr>
            </tbody>
        </>
    )
}


export default UserList;