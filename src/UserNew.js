import React, {Component} from "react";
import  { Redirect } from 'react-router-dom'
import  { NavLink } from 'react-router-dom'

class UserNew extends Component {
        constructor(props) {
            super(props);
            this.handleSubmit = this.handleSubmit.bind(this);
            this.handleChangeBase = this.handleChangeBase.bind(this);
            this.handleChangeConfirmPassword = this.handleChangeConfirmPassword.bind(this);
            this.handleChangeConfirmPassword2 = this.handleChangeConfirmPassword2.bind(this);
            this.state = {
                name: "",
                surname: "",
                confirmPassword: {
                    first: "",
                    second: ""
                },
                phoneNumber: "",
                email: ""
            }
        }

        handleChangeBase(e) {
            const value = e.target.value;
            const name = e.target.name;
            this.setState({
                [name]:value
            })
        }

    handleChangeConfirmPassword(e) {
        // const name = e.target.name;
        const value = e.target.value;
        this.setState({
            confirmPassword : {
                first : value,
                second: this.state.confirmPassword.second
            }
        })
    }

    handleChangeConfirmPassword2(e) {
        // const name = e.target.name;
        const value = e.target.value;
        this.setState({
            confirmPassword : {
              first: this.state.confirmPassword.first,
              second: value
            }
        })
    }

        handleSubmit(e){
            e.preventDefault();
            let json = JSON.stringify(this.state);
            fetch( "http://localhost:8000/api/user/new",{
                method: 'post',
                body: json,
                headers: new Headers({
                    Authorization:
                        "Bearer MmFhNGQxMzEzNTZiZWNiYjk4NGU3MWM4YzBhYTNhNDJlNDY4MDUxOTg0YThhYTQ1Yzk0MjQyZDkxZWVjNDU3Yw",
                        "Content-Type": "application/json"
                })
            })
            .then(response => {
                if (response.status === 201) {
                    window.location.replace("http://localhost:3001/user/");
                    // return <Redirect to="/user/"  />
                }
                // else {
                //     // response.error();
                //     throw new Error('Something went wrong ...')
                // }
            })
                .catch(error => console.log(error));
        }


    render() {
        return (
            <div className="d-flex justify-content-center">
                <div className="bg-secondary">
                    <h1 className="display-4 d-flex justify-content-center" style={{paddingTop: 30}}>Create new
                        User</h1>
                    <div className="d-flex justify-content-center" style={{paddingTop: 30}}>

                        <form onSubmit={this.handleSubmit}>
                            <div className="d-flex flex-column">

                                <label htmlFor="user">
                                    Name
                                    <input
                                        type="text"
                                        id="name"
                                        name="name"
                                        onChange={this.handleChangeBase}
                                    />
                                </label>

                                <label htmlFor="surname">
                                    Surname
                                    <input
                                        type="text"
                                        id="surname"
                                        name="surname"
                                        onChange={this.handleChangeBase}
                                    />
                                </label>
                                <label htmlFor="password">
                                    Password
                                    <input
                                        type="password"
                                        id="password"
                                        name="first"
                                        onChange={this.handleChangeConfirmPassword}
                                    />
                                </label>
                                <label htmlFor="password">
                                    Repeat Password
                                    <input
                                        type="password"
                                        id="password"
                                        name="second"
                                        onChange={this.handleChangeConfirmPassword2}
                                    />
                                </label>
                                <label htmlFor="phoneNumber">
                                    Phone Number
                                    <input
                                        type="text"
                                        id="phoneNumber"
                                        name="phoneNumber"
                                        onChange={this.handleChangeBase}
                                    />
                                </label>
                                <label htmlFor="email">
                                    E-mail
                                    <input
                                        type="email"
                                        id="email"
                                        name="email"
                                        onChange={this.handleChangeBase}
                                    />
                                </label>
                                <div className="text-center" style={{marginTop: 20}}>
                                    <input type="submit" value="Save" className="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="d-flex flex-column">
                        <NavLink to ="{{ path('user_index') }}" style={{textAlign: "center"}}>back to list</NavLink>
                        <NavLink to="{{ path('app_logout')}}" style={{textAlign: "center"}}>logout</NavLink>
                    </div>
                </div>
            </div>
        )
    }
}


export default UserNew