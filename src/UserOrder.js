import React from "react";
import {NavLink} from "react-router-dom";

const UserOrder = () => {
    return (
        <>
            <div className="container">
                <div className="text-center">
                    <h1 className="display-4">User order list</h1>
                </div>
                <table className="table table-striped table-hover table-sm table-responsive-sm" style={{marginTop: 30}}>
                    <thead className="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Number</th>
                        <th>Description</th>
                        <th>Comments</th>
                        <th>Headquarters</th>
                        <th>Package</th>
                        <th>SenderDetails</th>
                        <th>RecipientDetails</th>
                        <th>Status</th>
                        <th>Courier</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
                <div className="new">
                    <NavLink style={{display: "flex", justifyContent: "center"}} to="/user-order/new">Create
                        new</NavLink>
                </div>
            </div>
        </>
    )
}

export default UserOrder;