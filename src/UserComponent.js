import React from "react";
import User from "./User"
import UserNew from "./UserNew"
import UserOrder from "./UserOrder"
import UserOrderNew from "./UserOrderNew"
import CourierList from "./CourierList"
import CourierNew from "./CourierNew"
import CourierOrder from "./CourierOrder"
import LoginForm from "./LoginForm"
import {Route} from "react-router-dom";



const UserComponent = () => {
    return (
        <>
            <Route path="/login/" exact component={LoginForm}/>
            <Route path="/user/" exact component={User}/>
            <Route path="/user/new" exact component={UserNew}/>
            {/*<Route path="/user/{id}" exact component = {} />*/}
            {/*<Route path="/user/{id}/edit" exact component = {} />*/}
            <Route path="/user-order" exact component = {UserOrder} />
            <Route path="/user-order/new" exact component = {UserOrderNew} />
            <Route path="/courier/" exact component = {CourierList} />
            <Route path="/courier/new" exact component = {CourierNew} />
            <Route path="/courier-order" exact component = {CourierOrder} />
        </>
    )
};

export default UserComponent;
