import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import "./LoginForm.css";
class LoginForm extends Component {

    render() {
        return (
            <>
                <div className="container">
                    <div className="d-flex justify-content-center">
                        <div className="text-dark">
                            <form className="form-signin">

                                <div className="logo d-flex justify-content-center">
                                    <img src={'images/logo.png'} style={{width: 200}} alt=""/>
                                </div>
                                <h1 className="d-flex justify-content-center"
                                    style={{paddingTop: 30, fontSize: 20}}>Please sign in</h1>
                                <div className="d-flex justify-content-center" style={{paddingTop: 30}}>
                                    <label htmlFor="inputEmail" className="sr-only">Email address</label>
                                    <input type="email" value="last_username" name="email" id="inputEmail"
                                           className="form-control" placeholder="Email address" required autoFocus/>
                                    <label htmlFor="inputPassword" className="sr-only">Password</label>
                                </div>
                                <input type="password" name="password" id="inputPassword" className="form-control"
                                       placeholder="Password" required/>
                                {/*<input type="hidden" name="_csrf_token" value="{{ csrf_token('authenticate') }}"/>*/}
                                <div className="checkbox mb-3">
                                    <label>
                                        <input type="checkbox" value="remember-me"/> Remember me
                                    </label>
                                </div>
                                <div className="text-center">
                                    <button className="btn btn-primary" type="submit">
                                        Sign in
                                    </button>
                                </div>
                                <div className="text-center">
                                    <NavLink to="/user/new">Create User</NavLink>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default LoginForm;